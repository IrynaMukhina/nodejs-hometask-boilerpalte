const { fighter } = require('../models/fighter');
const FighterService = require('../services/fighterService');

const fighterModel = Object.assign({}, fighter);
delete fighterModel.id;

const createFighterValid = (req, res, next) => {
  const error = validateFighterFields(req.body, 'creation');

    if (error.exist) {
      return res.status(400).send({
        error: true,
        message: error.message
      });
    }

    next()
}

const updateFighterValid = (req, res, next) => {
  const error = validateFighterFields(req.body);
    if (error.exist) {
      return res.status(400).send({
        error: true,
        message: error.message
      });
    }

    next()
}

function validateFighterFields(reqBody, type = 'updating') {
  const { name, health, power, defense } = reqBody;
  const emptyFields = checkAndReturnEmptyFields(reqBody);
  const fighterNameAlreadyExist = FighterService.getAll().some(fighter => fighter.name === name);

  let error = {
    exist: false,
    message: ''
  };

  if (emptyFields.length) {
    error.exist = true;

    error.message += `Field${emptyFields.length > 1 ? 's' : ''} ${emptyFields.join(', ')} ${emptyFields.length > 1 ? 'are' : 'is'} empty. All fields are required.\n`;
  }

  if (type === 'creation') {
    if (fighterNameAlreadyExist) {
      error.exist = true;

      error.message += `Fighter with sych name have already exist.\n`;
    }
  }

  console.log(typeof power);
  

  if (typeof power !== 'number' || typeof defense !== 'number') {
    error.exist = true;

    error.message += `Fields 'power', 'health' and 'defense' require only numbers.\n`;
  }

  return error;
}

function checkAndReturnEmptyFields(reqBody) {
  const emptyFields = [];

  for (let field in reqBody) {
    if (reqBody[field].length === 0) {
      emptyFields.push(field);
    }
  }

  return emptyFields;
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;