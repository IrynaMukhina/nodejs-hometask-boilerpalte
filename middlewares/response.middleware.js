const responseMiddleware = (req, res, next) => {
  if (res.data) {
    res.status(200).send(res.data);
  }

  if (res.err) {
    res.status(400).send({
      error: true,
      message: res.err.toString()
    })
  }
}

exports.responseMiddleware = responseMiddleware;