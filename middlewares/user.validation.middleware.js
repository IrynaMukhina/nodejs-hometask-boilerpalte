const { user } = require('../models/user');
const UserService = require('../services/userService');

const createUserValid = (req, res, next) => {
    const error = validateUserFields(req.body, 'creation');

    if (error.exist) {
      return res.status(400).send({
        error: true,
        message: error.message
      });
    }

    next()
}

const updateUserValid = (req, res, next) => {
  const error = validateUserFields(req.body);

  if (error.exist) {
    return res.status(400).send({
      error: true,
      message: error.message
    });
  }

  next()
}

function checkAndReturnEmptyFields(reqBody) {
  const emptyFields = [];
  
  for (let field in reqBody) {
    if (reqBody[field].length === 0) {
      emptyFields.push(field);
    }
  }

  return emptyFields;
}

function isEmailValid(email) {
  const gmailRegExp = new RegExp('([a-zA-Z0-9]+)([\.{1}])?([a-zA-Z0-9]+)\@gmail([\.])com', 'g');

  return gmailRegExp.test(email);
}

function isPhoneNumberValid(phone) {
  const phoneRegExp = new RegExp('^\\+380[0-9]{9}$', 'g');

  return phoneRegExp.test(phone);
}

function validateUserFields(reqBody, type = 'updating') {
  const { firstName, lastName, email, phoneNumber, password } = reqBody;
  const isEmailExist = UserService.getAll().some(user => user.email === email);
  const isPhoneExist = UserService.getAll().some(user => user.phoneNumber === phoneNumber);
  const emptyFields = checkAndReturnEmptyFields(reqBody);
  const isEmailAddressValid = isEmailValid(email);
  const isPhoneValid = isPhoneNumberValid(phoneNumber);

  let error = {
    exist: false,
    message: ''
  };

  if (type === 'creation') {
    if (isEmailExist) {
      error.exist = true;

      error.message += `User with such email have already exist\n`;
    }

    if (isPhoneExist) {
      error.exist = true;

      error.message += `Such phone number have already exist\n`;
    }
  }

  if (emptyFields.length) {
    error.exist = true;

    error.message += `Field${emptyFields.length > 1 ? 's' : ''} ${emptyFields.join(', ')} ${emptyFields.length > 1 ? 'are' : 'is'} empty. All fields are required.\n`
  }

  if (!isEmailAddressValid) {
    error.exist = true;

    error.message += `Only gmail mail address valid.\n`;
  }

  if(!isPhoneValid) {
    error.exist = true;

    error.message += `Phone number should start from '+380' it's length should be 9 numbers.\n`;
  }
  
  return error;
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;