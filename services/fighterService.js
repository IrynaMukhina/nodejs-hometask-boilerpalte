const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {

  getAll() {
    const fighters = FighterRepository.getAll();

    if (!fighters) {
      return null;
    }

    return fighters;
  }

  getOne(id) {
    const fighter = FighterRepository.getOne({ id });    

    if (!fighter) {
      return null;
    }

    return fighter;
  }

  create(fighterBody) {
    const fighter = FighterRepository.create(fighterBody);

    if (!fighter) {
      return null;
    }

    return fighter;
  }

  update(id, newfighterBody) {
    const updatedfighter = FighterRepository.update(id, newfighterBody);

    if (!updatedfighter) {
      return null;
    }

    return updatedfighter;
  }

  delete(id) {
    const deletedfighter = FighterRepository.delete(id)

    if (!deletedfighter) {
      return null;
    }

    return deletedfighter;
  }
}

module.exports = new FighterService();