const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    search(search) {
        const item = UserRepository.getOne(search);

        if(!item) {
            return null;
        }
        return item;
    }

    getAll() {
        const users = UserRepository.getAll();

        if (!users) {
            return null;
        }
        
        return users;
    }

    getOne(id) {
        const user = UserRepository.getOne({ id });

        if (!user) {
            return null;
        }

        return user;
    }

    create(userBody) {
        const user = UserRepository.create(userBody);

        if (!user) {
            return null;
        }

        return user;
    }

    update(id, newUserBody) {
        const updatedUser = UserRepository.update(id, newUserBody);

        if (!updatedUser) {
            return null;
        }

        return updatedUser;
    }

    delete(id) {
        const deletedUser = UserRepository.delete(id)

        if (!deletedUser) {
            return null;
        }

        return deletedUser;
    }
}

module.exports = new UserService();