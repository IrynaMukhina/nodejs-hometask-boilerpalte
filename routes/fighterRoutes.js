const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();


router.get('/', (req, res, next) => {
  try {
    res.data = FighterService.getAll();
  } catch (e) {
    res.err = e;
  } finally {
    next()
  }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
  try {
    const { id: fighterId } = req.params;
    
    const fighter = FighterService.getOne(fighterId);

    if (fighter) {
      res.data = fighter;
    } else {
      return res.status(404).send({
        error: true,
        message: 'Fighter not found'
      });
    }
  } catch (e) {
    res.err = e;
  } finally {
    next()
  }
}, responseMiddleware);

router.post('/', createFighterValid, (req, res, next) => {
  try {
    res.data = FighterService.create(req.body);
  } catch (e) {
    res.err = e;
  } finally {
    next()
  }
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
  try {
    const { id: fighterId } = req.params;
    const newFighterBody = req.body;

    const fighter = FighterService.update(fighterId, newFighterBody);

    if (fighter) {
      res.data = fighter;
    } else {
      return res.status(404).send({
        error: true,
        message: 'Fighter not found'
      });
    }
  } catch (e) {
    res.err = e;
  } finally {
    next()
  }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
  try {
    const { id: fighterId } = req.params;

    const fighter = FighterService.delete(fighterId);

    if (fighter) {
      res.data = fighter;
    } else {
      return res.status(404).send({
        error: true,
        message: 'Fighter not found'
      });
    }
  } catch (e) {
    res.err = e;
  } finally {
    next()
  }
}, responseMiddleware);


module.exports = router;
