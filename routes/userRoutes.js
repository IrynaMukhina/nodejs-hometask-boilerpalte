const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { user } = require('../models/user');

const router = Router();

const userModel = Object.assign({}, user);
delete userModel.id;

router.get('/', (req, res, next) => {
  try {    
    res.data = UserService.getAll();
  } catch (e) {
    res.err = e;
  } finally {
    next()
  }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
  try { 
    const { id: userId } = req.params;   

    const user = UserService.getOne(userId);
    
    if (user) {
      res.data = user;
    } else {
      return res.status(404).send({
        error: true,
        message: 'User not found'
      });
    }
  } catch (e) {
    res.err = e;
  } finally {
    next()
  }
}, responseMiddleware);

router.post('/', createUserValid, (req, res, next) => {
  try {
    res.data = UserService.create(req.body);
  } catch (e) {
    res.err = e;
  } finally {
    next()
  }
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {
  try {
    const { id: userId } = req.params;;
    const newUserBody = req.body;

    const user = UserService.update(userId, newUserBody);

    if (user.lenght) {
      res.data = user;
    } else {
      return res.status(404).send({
        error: true,
        message: 'User not found'
      });
    }
  } catch (e) {
    res.err = e;
  } finally {
    next()
  }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
  try {
    const { id: userId } = req.params;;

    const user = UserService.delete(userId);

    if (user.lenght) {
      res.data = user;
    } else {
      return res.status(404).send({
        error: true,
        message: 'User not found'
      });
    }
  } catch (e) {
    res.err = e;
  } finally {
    next()
  }
}, responseMiddleware);

module.exports = router;
